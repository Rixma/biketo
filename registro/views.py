from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import SignUpForm
from .models import Persona

# Create your views here.

def welcome(request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        return render(request,'index.html', {})
    # En otro caso redireccionamos al login
    return redirect('/login')

class SignUpView(CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'commons/signup.html'

def login(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "login.html", {'form': form})

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('/')

def index(request):
    return render(
        request,
        'index.html',
        {} 
    )
  
    
def mapa(request):
    return render(
        request,
        'map.html',
        {}
    )

def registrar(request):
    return render(
        request,
        'formulario.html'

    )

def crear(request):
    nombre = request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    correo = request.POST.get('correo','')
    fechanac = request.POST.get('fechanac','')

    return HttpResponse("nombre: "+nombre+" apellido: "+apellido+" correo: "+correo+" fecha de nacimiento: "+fechanac)

