from django.db import models

# Create your models here.

class Persona(models.Model):
    nombre: models.CharField(max_length=20)
    apellido: models.CharField(max_length=20)
    correo: models.CharField(max_length=40)
    fechanac: models.DateField()
    def __str__(self):
        return "nombre: "+self.nombre+" apellido: "+self.apellido+" correo: "+self.correo+" fecha de nacimiento: "+self.fechanac
    


